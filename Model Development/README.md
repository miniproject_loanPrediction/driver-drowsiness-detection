# Model Development of Driver Drowsiness Detection 

## Description
For Driver Drowsiness Detection, we collected around 3000 images in total. It contains three different classes; open eyes, closed eyes and unknown as this project detects the drowsiness of a driver based on the eye closure of the user.

Characteristics of the dataset are as follows:
- The dataset contains a total of 2700 images in three categories.
- Each category has 900 images.
- The dataset is balanced.
- Class Labels – ‘Open Eye’, ‘Closed Eye’ and ‘Unknown’.
- Class Labels were encoded such that 0 represents close Eye, 1 illustrates open Eye and 2 represents Unknown images.

## Model development
We implemented a custom- designed Convolutional Neural Network that has the following Layers:
- An input layer.
- 5 Convolutional layers.
- 5 Maxpooling layers.
- Fully Connected Layers follow Convolution Layers for classification.
- A Dense Layer
ImageDataGenerator has been used for randomizing the training images for better performance of the model.
The following image shows a snippet of the model developed:
<br>
<img src="modelImages/1.png" width="500" height="200" />
<br>

## Model Training
Model training runs for a total of 100 epochs with a batch size of 32. Early stopping has been used for controlling the model training that is if the training accuracy doesn’t alter even after running certain epochs then earlystopping will stop the model training. 
Snippet of the model being trained is shown below:
<br>
<img src="modelImages/2.png" width="400" height="200">
<br>

## Model Evaluation
After training the model, it is important to evaluate the model to see how it performs. 
The accuracy of train, test and valid data is shown below:
<br>
<img src="modelImages/3.png" width="400" height="200">
<br>
As shown above, the accuracy of the model for train, test and valid data is 99% which is pretty much good. From here we got to know that our model is ready to be deployed in our mobile application for drowsiness detection. 


