# ITPRJ303 Project III
*Gyalpozhing College of Information Technology*
## About the project 

This project is intended to be used as a teaching case study in PRJ303, in sixth semester course. It is aimed at the studnets of Gyalpozhing College of Information Technology to learn how to use data analytics technologies and the big data life cycle to extract features, pre-process, clean, analyze, interpret, and visualize large real-world raw datasets.

## Group 3
## Topic of project - Driver Drowsiness Detection
The objective of developing this machine learning model and mobile application is because of the accidents caused by drivers who are drowsy. It is becoming one of the leading causes of traffic accidents in Bhutan. 
In this project, we present a model for driver drowsiness detection. In this, the driver is continuously monitored through mobile app. This model uses image processing techniques which mainly focuses on eye closure of the driver. The model extracts the drivers face and predicts the blinking of eye from eye region. If the blinking rate is high, then the system alerts the driver with an alarm.

## Project Team Members

Project Guide - Mr. Ji Piengxang
<br>
<img src="Images/sir.png" width="200" height="200" />
<br>
Project Leader - Mrs. Cheki Lhamo (12190043)
<br>
<img src="Images/cheki.png" width="200" height="200" />
<br>
Programmer - Mr. Kezang Dorji (12190059)
<br>
<img src="Images/kezang.png" width="200" height="200" />
<br>
UI/UX Designer - Mrs. Thinley Wangmo (12190089)
<br>
<img src="Images/tley.png" width="200" height="200" />
<br>
Lead Programmer - Mr. Tshering Wangchuk (12190099)
<br>
<img src="Images/tshering.png" width="200" height="200" />
<br>


## Click the links given below:
To view app folder :
(https://gitlab.com/miniproject_loanPrediction/driver-drowsiness-detection/-/tree/main/Mobile%20Application)
<br>
<br>
To view machine learning model folder:
(https://gitlab.com/miniproject_loanPrediction/driver-drowsiness-detection/-/tree/main/Model%20Development)
<br>
<br>
Click Here to watch promotional Video:
(https://youtu.be/CDIeRgd_5qc)
<br>
<br>

