package org.tensorflow.lite.examples.classification;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AboutFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter programAdapter;
    RecyclerView.LayoutManager layoutManager;
    String[] title = {"Project Guide", "Group Leader", "Programmer",
            "UI Designer", "Programmer"};
    String[] titleDescription = {"Mr. Ji Peng Xiang", "Cheki Lhamo",
            "Kezang Dorji", "Thinley Wangmo", "Tshering Wangchuk"};

    int[] programImages = {R.drawable.sir, R.drawable.cheki, R.drawable.kezang,
            R.drawable.tley, R.drawable.humble};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rvProgram);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        programAdapter = new ProgramAdapter(getContext(), title, titleDescription, programImages);
        recyclerView.setAdapter(programAdapter);
        return view;
    }
}